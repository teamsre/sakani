<?php

// we copy all the files under this directory to the vm, mapping the root of the copy
// dir to the OS root dir.
// e.g. /foo/bar/copy_dir/some/file => /some/file
$copyToVmDir = __DIR__.'/copy_to_vm';

// services we restart after files are copied
$servicesToRestart = ['apache2'];

// local dev database credentials
$localDb         = 'sakani';
$localDbUsername = 'sakani';
$localDbPassword = 'sakani';

// --- END SCRIPT CONFIG ---


echo ">>> Start Post-Login Provisioning Script\n";

echo ">>> Copying files from `{$copyToVmDir}` to root directory\n";

// create an array of all files in the copy dir recursively
$files = [];

function find_files($dirName)
{
  $prefix = $dirName . '/';
  $dir    = dir($dirName);
  global $files;

  while (false !== ($file = $dir->read()))
  {
    if ($file !== '.' && $file !== '..')
    {
      $file = $prefix . $file;

      if (is_dir($file)) find_files($file);
      else               $files[] = $file;
    }
  }
}

find_files($copyToVmDir);

// copy each found file from the copy dir to the OS root dir,
// keeping its relative path
foreach ($files as $file)
{
  $toDir = dirname(str_replace($copyToVmDir, '', $file));

  shell_exec("sudo cp -f {$file} {$toDir}");
}

  echo ">>> Enabling virtual hosts\n";
  
  shell_exec("sudo a2ensite one.sakani.dev");
  shell_exec("sudo a2ensite panel.one.sakani.dev");
  shell_exec("sudo a2ensite sakani.dev");
  shell_exec("sudo a2ensite panel.sakani.dev");
  shell_exec("sudo a2ensite one.sakani.dev-ssl");
  shell_exec("sudo a2ensite panel.one.sakani.dev-ssl");
  shell_exec("sudo a2ensite sakani.dev-ssl");
  shell_exec("sudo a2ensite panel.sakani.dev-ssl");

  echo ">>> Add localhost Servername to apache2.conf for removing localhost error \n";
  shell_exec(" echo 'ServerName localhost' |sudo tee -a  /etc/apache2/apache2.conf");

echo ">>> Enabling PHP short tag\n";
shell_exec("sudo sed -i 's/short_open_tag = Off/short_open_tag = On/g' /etc/php5/apache2/php.ini");

echo ">>> Restarting services\n";

foreach ($servicesToRestart as $service) shell_exec("sudo service {$service} restart");

echo ">>> Composer Install\n";

`composer install`;

echo ">>> NPM Install\n";

`sudo npm install`;

echo ">>> Creating Dev DB and DB user\n";

shell_exec("mysql -uroot -proot -e \"CREATE DATABASE {$localDb}\"");
shell_exec("mysql -uroot -proot -e \"CREATE USER '{$localDbUsername}'@'%' IDENTIFIED BY '{$localDbPassword}'\"");
shell_exec("mysql -uroot -proot -e \"GRANT ALL PRIVILEGES ON {$localDb}.* TO '{$localDbUsername}'@'%'\"");

echo ">>> Copying Laravel .env.example to .env and Updating Dev DB credentials\n";

`cp .env.example .env`; // copy

// update credentials
shell_exec("sed -i 's/DB_DATABASE=homestead/DB_DATABASE={$localDb}/' .env");
shell_exec("sed -i 's/DB_USERNAME=homestead/DB_USERNAME={$localDbUsername}/' .env");
shell_exec("sed -i 's/DB_PASSWORD=secret/DB_PASSWORD={$localDbPassword}/' .env");

echo ">>> Generating local app key\n";

`php artisan key:generate`;


echo ">>> Creating SQL dump file from remote Beta production DB\n";
$productionDumpFilePath = 'production_dump_'.date('Y_m_d').'.sql';


echo ">>>  Production database username: \n";
$productionDbUsername = read_stdin();

echo ">>> Production database password: \n";
//Hiding user input display for password protection
system('stty -echo');

$productionDbPassword = read_stdin();

//Restoring  hide input
system('stty echo');

echo ">>> Please wait... While your DB is generating backup file\n";
shell_exec("mysqldump -u {$productionDbUsername} -p{$productionDbPassword} -h sakani.com sakani_beta > {$productionDumpFilePath}");

echo ">>> Building local DB tables and data from production dump\n";
shell_exec("mysql -u{$localDbUsername} -p{$localDbPassword} {$localDb} < {$productionDumpFilePath}");

echo ">>> Deleting SQL dump file\n";

shell_exec("rm {$productionDumpFilePath}");

/**
 * Simple method for read input from console
 */
function read_stdin()
{
  $fr=fopen("php://stdin","r");   // open our file pointer to read from stdin
  $input = fgets($fr,128);        // read a maximum of 128 characters
  $input = rtrim($input);         // trim any trailing spaces.
  fclose ($fr);                   // close the file handle
  return $input;                  // return the text entered
}

