#!/usr/bin/env bash

echo ">>> Starting Install Script"

# Update
sudo apt-get update

# Install MySQL without prompt
sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password password root'
sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password root'

echo ">>> Installing Base Items"

# Install base items
sudo apt-get update && sudo apt-get install -y curl wget build-essential python-software-properties

echo ">>> Adding PPA's and Installing Server Items"

# Add repo for latest PHP
sudo add-apt-repository ppa:ondrej/php5-5.6

# Add rep and key for NGINX
#echo "deb http://ppa.launchpad.net/nginx/stable/ubuntu $(lsb_release -sc) main" | sudo tee /etc/apt/sources.list.d/nginx-stable.list
#sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys C300EE8C

# Update Again
sudo apt-get update

# Install Git, Apache, PHP, and MySQL
sudo apt-get install -y  git-core php5 apache2 libapache2-mod-php5 php5-mysql php5-curl php5-gd php5-mcrypt mysql-server php5-sqlite sqlite3


echo ">>> Installing necessary packages"
sudo apt-get install -y imagemagick	
sudo apt-get install -y imagemagick-common	
sudo apt-get install -y php5-imagick


echo ">>> Enable apache proxy"
sudo a2enmod proxy_http
sudo a2enmod proxy

echo ">>> Enable ssl module"
sudo a2enmod ssl

echo ">>> Creating ssl directory";
sudo mkdir /etc/apache2/ssl

echo ">>> Generating key and certificate for ssl certificate";
# Certificate parameters
# Required
commonname=sakani.dev

# Change to your company details
country=SA
state=Jeddah
locality=Jeddah
organization=sakani
organizationalunit=PHP_DEV
email=admin@sakani.com

#Generate a key and certificate
sudo openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/apache2/ssl/apache.key -out /etc/apache2/ssl/apache.crt \
-subj "/C=$country/ST=$state/L=$locality/O=$organization/OU=$organizationalunit/CN=$commonname/emailAddress=$email"

echo ">>> Enable apache mod rewrite"
sudo a2enmod rewrite

sudo service apache2 restart

echo ">>> Remove exisiting nodejs and npm packages"
sudo apt-get remove nodejs ^node-* nodejs-*
sudo apt-get autoremove
sudo apt-get autoclean

# Note the new setup script name for Node.js v0.12
curl -sL https://deb.nodesource.com/setup_0.12 | sudo bash -
# Then install with:
sudo apt-get install -y nodejs

echo ">>> starting apache server"

sudo service apache2 start

echo ">>> Installing Composer"

# Composer
curl -sS https://getcomposer.org/installer | php
sudo mv composer.phar /usr/local/bin/composer

echo ">>> Update PPAs and Install Node & NPM"

# Update again
curl -sL https://deb.nodesource.com/setup | sudo bash -

# Install Node
sudo apt-get -y install nodejs

echo ">>> Enabling swap memory on the VM"

sudo /bin/dd if=/dev/zero of=/var/swap.1 bs=1M count=1024
sudo /sbin/mkswap /var/swap.1
sudo /sbin/swapon /var/swap.1