<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BuildFromBetaProductionDump extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    echo ">>> Creating SQL dump file from remote Beta production DB\n";

    $productionDumpFilePath = storage_path().'/database/production_dump_'.date('Y_m_d').'.sql';

    $productionDbUsername = env('DB_PRODUCTION_USERNAME');
    $productionDbPassword = env('DB_PRODUCTION_PASSWORD');

    shell_exec("mysqldump -u {$productionDbUsername} -p{$productionDbPassword} -h sakani.com sakani_beta > {$productionDumpFilePath}");

    echo ">>> Building local DB tables and data from production dump\n";

    $localDb         = env('DB_DATABASE');
    $localDbUsername = env('DB_USERNAME');
    $localDbPassword = env('DB_PASSWORD');

    shell_exec("mysql -u{$localDbUsername} -p{$localDbPassword} {$localDb} < {$productionDumpFilePath}");

    echo ">>> Deleting SQL dump file\n";

    shell_exec("rm {$productionDumpFilePath}");

    echo ">>> Dropping table laravel_migrations (old migrations table)\n";

    Schema::drop('laravel_migrations');
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    $tables = [];

    foreach (DB::select('SHOW TABLES') as $k => $v)
    {
      $tables[] = array_values((array)$v)[0];
    }

    foreach($tables as $table)
    {
      if ($table !== 'migrations')
      {
        echo ">>> Dropping table {$table}\n";

        Schema::drop($table);
      }
    }
  }
}
