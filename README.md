# Hello and Welcome to the Sakani app!
Sakani is going to be migrated to Laravel 5. We are planning a gradual migration from beta to 1.0 version. 

So we have to keep both beta and new version in same server. Here we are going to explain how to setup sakani development environment by using `vagrant` machine. 

This repository includes the 1.0 modules:

- Sakani Search - Allows searchers to find properties and contact property representatives.
- Sakani Panel  - Allows administrators to manage multiple "behind the scenes" facets of Sakani
                  Search, including property and client management.

## Getting Started
To get developing on the Sakani app, you have to setup your development environment.

### Setting Up Your Development Environment 

#### Your Vagrant Virtual Machine

The first thing you have to do is download and install [Virtualbox](https://www.virtualbox.org/wiki/Downloads) and [Vagrant](https://docs.vagrantup.com/v2/installation/index.html) if you don't have them on your dev machine already.

You'll want to setup both 1.0 and beta apps to work side-by-side. We'll assume you already have the beta app repos ([`sakani_beta`](https://bitbucket.org/teamsre/sakani_beta), [`sakani_beta_panel`](https://bitbucket.org/teamsre/sakani_beta_panel), and [`sakani_beta_common`](https://bitbucket.org/teamsre/sakani_beta_common)) If not, pull those repos in first.

> We'll assume that all your repo directories reside 
> in your home (`~`) directory. If not, just make
> the appropriate changes in your local paths as you 
> work along.

Once you've pulled in the beta repos and have Virtualbox and Vagrant installed, you need to create a parent directory for hosting all the repos in a single directory. Make the `code` directory somewhere on your file system.

    $ mkdir code

Navigate into the subdirectory and clone the sakani 1.0 code base from this [bitbucket repo](https://bitbucket.org/teamsre/sakani/).


    $ git clone git@bitbucket.org:teamsre/sakani.git

This will create a directory `sakani` in your host machine with sakani 1.0 code base.
Navigate into `sakani` directory and run the `vagrant` command.

    $ vagrant up

It might take a while to setup your local development virtual machine (VM). A Linux VM box will be downloaded if necessary. After that, the box will be copied and provisioned to create the Sakani app dev environment.

#### Bring the `sakani_beta` Application Into the Machine


Copy sakani beta repos into the `code` directory.

     cp -rf sakani_beta ~/code/
     cp -rf sakani_beta_common ~/code/
     cp -rf sakani_beta_panel ~/code/

After this step our all code base directories are under one directory named `code` in the host machine.

#### Logging in to Your VM and Post-Login Provisioning


Once the VM is provisioned and started, you can log in to your dev VM by navigating to your project root in your terminal and typing the following.

    $ vagrant ssh

Your terminal prompt should change to reflect the fact that you're now in your VM. Navigate to the `/vagrant/code/sakani` directory in your VM. This directory is automatically synced with your project directory by Vagrant. Once you're there, run the following.

    vagrant...$ php vm_provisioning/post_login_provision.php

This script will take care of dev environment setup that was not possible before SSH login.

> As a rule of thumb, never run provisioning scripts on the staging or production servers.
> You could seriously muck up the live servers that way. Of course, on your local machine it's
> not much of a worry. Worst case, you can destory the dev VM and start over without any harm
> to your host machine.


After running the post-login provisioning script your vagrant is almost ready to use with Laravel 5 and Laravel 3 as well. The script will take care of following things:

* Create all virtual hosts with ssl enabled, necessary virtual host files are copied to `/etc/apache2/sites-available`. Enabled sites are:

    1. one.sakani.dev
    2. panel.one.sakani.dev
    3. sakani.dev
    4. panel.sakani.dev

* Create database named `sakani` - Please refer the script or contact a sakani developer for default DB credentials.
* Install all PHP vendor packages via composer
* Install all Node packages via NPM

You can access the sites by using following urls:

    http://one.sakani.dev:8000
    https://one.sakani.dev:44300


Please note the `8000`, we are forwarding default port `80` to `8000` in vagrant. For ssl enabled domain we are forwarding `443` to `44300`.







